import express from 'express'
import { getTickets } from '../controller/ticketController'
import protect from '../middleware/authMiddleware'

const router = express.Router()

router.route('/').get(protect, getTickets)

// router.route('/').get().post()

export default router
