import express from 'express'
import { getMe, login, registerUser } from '../controller/userController'

const router = express.Router()

router.post('/register', registerUser)
router.post('/login', login)

router.get('/me', getMe)

export default router
