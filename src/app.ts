import 'dotenv/config'
import express from 'express'
import { errorHandler } from './middleware/errorMiddleWare'

import ticketRouter from './routes/tickerRoutes'
import userRouter from './routes/userRoutes'

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/api/v1/users', userRouter)
app.use('/api/v1/tickets', ticketRouter)

app.use(errorHandler)

export default app
