import { Request, Response } from 'express'
import { catchAsync } from '../utils/catchAsync'

type CustomRequest = Request & {
  user?: any
}

export const getTickets = catchAsync(
  async (req: CustomRequest, res: Response) => {
    const user = {
      user_id: req.user.user_id,
      name: req.user.name,
      email: req.user.email,
    }
    res.status(200).json(user)
  }
)
