import bcrypt from 'bcrypt'
import { Request, Response } from 'express'
import pool from '../config/db'
import { catchAsync } from '../utils/catchAsync'

import jwt from 'jsonwebtoken'
import { TLoginUser, TUser } from '../types/requestTypes'

type IAuthUser = Request & {
  user?: TUser
}

export const registerUser = catchAsync(
  async (req: Request<{}, {}, TUser>, res: Response) => {
    let { name, email, password } = req.body

    if (!name || !email || !password) {
      res.status(400)
      throw new Error('Please include all fields')
    }

    const getUserEmail = {
      name: 'fetch-user',
      text: 'SELECT * FROM users WHERE email = $1',
      values: [email],
    }

    const userExists = await pool.query(getUserEmail)

    if (userExists.rowCount) {
      res.status(400)
      throw new Error('User already exists')
    }

    const salt = await bcrypt.genSalt(10)
    password = await bcrypt.hash(password, salt)

    const createUserQuery = {
      name: 'new-user',
      text: 'INSERT INTO users(name, email,password) VALUES($1, $2, $3) RETURNING *',
      values: [name, email, password],
    }

    const response = await pool.query(createUserQuery)
    res.status(200).json({
      status: 'success',
      data: {
        ...response.rows[0],
        token: generateToken(response.rows[0].user_id),
      },
    })

    console.log(response.rows[0])
  }
)

export const login = catchAsync(
  async (req: Request<{}, {}, TLoginUser>, res: Response) => {
    const { email, password } = req.body

    const checkUserEmail = {
      name: 'fetch-user',
      text: 'SELECT * FROM users WHERE email = $1',
      values: [email],
    }

    const user = await pool.query(checkUserEmail)

    const confirPassword = await bcrypt.compare(password, user.rows[0].password)

    if (user.rowCount && confirPassword) {
      res.status(200).json({
        status: 'success',
        data: {
          email: user.rows[0].email,
          name: user.rows[0].name,
          is_admin: user.rows[0].is_admin,
          token: generateToken(user.rows[0].user_id),
        },
      })
    } else {
      res.status(401)
      throw new Error('Invalid Creadentials')
    }
  }
)

export const getMe = (req: IAuthUser, res: Response) => {
  console.log(req.user)
  res.status(200).json('hello')
}

const generateToken = (user_id: number) => {
  if (!process.env.JWT_SECRET) {
    throw new Error('JWT secret is not defined')
  }
  return jwt.sign({ user_id }, process.env.JWT_SECRET, { expiresIn: '30d' })
}
