import pg from 'pg'
const { Pool } = pg

const pool = new Pool({
  host: 'localhost',
  database: 'supportdeskdb',
  user: 'postgres',
  password: '',
  port: 5432,
})

export default pool
