import jwt, { JwtPayload } from 'jsonwebtoken'

import { NextFunction, Request, Response } from 'express'
import pool from '../config/db'
import { catchAsync } from '../utils/catchAsync'
import { TUser } from '../types/requestTypes'

type IAuthUser = Request & {
  user?: TUser
}

const protect = catchAsync(
  async (req: IAuthUser, res: Response, next: NextFunction) => {
    let token: string = ''
    const authorization = req.headers.authorization
    if (authorization && authorization.startsWith('Bearer')) {
      try {
        token = authorization.split(' ')[1]
        if (!process.env.JWT_SECRET) {
          throw new Error('JWT secret is not defined')
        }
        const decode = jwt.verify(token, process.env.JWT_SECRET) as JwtPayload
        const query = {
          name: 'fetch-user',
          text: 'SELECT * FROM users WHERE user_id = $1',
          values: [decode?.user_id],
        }
        const { rows } = await pool.query(query)
        req.user = rows[0]
        next()
      } catch (error) {
        res.status(401)
        throw new Error('Not Authorized')
      }
    }
    if (!token) {
      res.status(401)
      throw new Error('Not Authorized')
    }
  }
)

export default protect
