export type TUser = {
  name: string
  email: string
  password: string
}

export type TLoginUser = Omit<TUser, 'name'>
